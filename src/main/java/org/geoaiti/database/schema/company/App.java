package org.geoaiti.database.schema.company;

import jakarta.persistence.*;
import lombok.*;
import org.geoaiti.database.schema.global.IdentityRoot;
import org.geoaiti.database.util.Auditable;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "companies")
public class App extends Auditable {

    private Long size;
    private Long downloads;
    private Long releaseDate;
    private Double userRating;
    private Double price;

    @OneToMany(mappedBy = "app")
    private List<AppTranslator> translators;

    @OneToMany(mappedBy = "app")
    private List<IdentityRoot> identities;

}
