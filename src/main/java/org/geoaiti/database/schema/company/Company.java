package org.geoaiti.database.schema.company;

import jakarta.persistence.*;
import lombok.*;
import org.geoaiti.database.schema.global.IdentityRoot;
import org.geoaiti.database.util.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "companies")
public class Company extends Auditable {

    private String website;
    private String email;
    private String phoneNumber;
    private String logoUrl;
    private String industry;
    private String sector;

    private Long establishedDate;

    @Enumerated(EnumType.STRING)
    private CompanyType type;

    @Enumerated(EnumType.STRING)
    private CompanySize size;

    @Embedded
    private CompanyAddress headquarters;

    @ElementCollection
    @CollectionTable(joinColumns = @JoinColumn, schema = "companies")
    private List<CompanyAddress> locations;

    @ElementCollection
    @CollectionTable(joinColumns = @JoinColumn, schema = "companies")
    private List<CompanySocialMediaLink> socialMedia;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CompanyEmployee> companyEmployees;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BusinessUnit> businessUnits;

    @ManyToOne
    @JoinColumn
    private Company parentCompany;

    @OneToMany(mappedBy = "parentCompany", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Company> subsidiaries;

    @OneToMany(mappedBy = "company")
    private List<CompanyTranslator> translators;

    @OneToMany(mappedBy = "company")
    private List<IdentityRoot> identities;

}
