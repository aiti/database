package org.geoaiti.database.schema.company;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;
import org.geoaiti.database.util.Auditable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "companies")
public class CompanyEmployee extends Auditable {

    private String name;
    private String email;
    private String phoneNumber;
    private String jobTitle;
    private String department;

    private Long dateOfBirth;
    private Long hireDate;

    @ManyToOne
    @JoinColumn
    private Company company;

}
