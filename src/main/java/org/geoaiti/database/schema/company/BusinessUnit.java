package org.geoaiti.database.schema.company;

import jakarta.persistence.*;
import lombok.*;
import org.geoaiti.database.util.Auditable;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "companies")
public class BusinessUnit extends Auditable {

    @ManyToOne
    @JoinColumn
    private Company company;

    @OneToMany(mappedBy = "businessUnit")
    private List<BusinessUnitTranslator> translators;

}
