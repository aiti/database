package org.geoaiti.database.schema.company;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;
import org.geoaiti.database.util.NameDescriptionTranslator;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "companies")
public class AppTranslator extends NameDescriptionTranslator {

    private String version;
    private String developer;
    private String platform;
    private String programmingLanguage;
    private String mainFeatures;

    @ManyToOne
    @JoinColumn
    private App app;

}
