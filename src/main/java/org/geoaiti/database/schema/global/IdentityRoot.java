package org.geoaiti.database.schema.global;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;
import org.geoaiti.database.schema.company.App;
import org.geoaiti.database.schema.company.Company;
import org.geoaiti.database.schema.user.User;
import org.geoaiti.database.util.Id;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "public")
public class IdentityRoot extends Id {

    @ManyToOne
    @JoinColumn
    private App app;

    @ManyToOne
    @JoinColumn
    private Company company;

    @ManyToOne
    @JoinColumn
    private User user;

}
