package org.geoaiti.database.schema.user;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.*;
import org.geoaiti.database.util.Auditable;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "users")
public class Role extends Auditable {

    private Integer count_user;

    @OneToMany(mappedBy = "role")
    private List<RoleTranslator> translators;

    @OneToMany(mappedBy = "role")
    private List<RolePermission> rolePermissions;

    @OneToMany(mappedBy = "role")
    private List<UserRole> userRoles;

}
