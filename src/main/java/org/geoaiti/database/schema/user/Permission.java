package org.geoaiti.database.schema.user;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.*;
import org.geoaiti.database.util.Auditable;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "users")
public class Permission extends Auditable {

    private boolean isActive;

    @OneToMany(mappedBy = "permission")
    private List<PermissionTranslator> translators;

    @OneToMany(mappedBy = "permission")
    private List<RolePermission> rolePermissions;

}
