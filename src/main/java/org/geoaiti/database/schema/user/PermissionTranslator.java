package org.geoaiti.database.schema.user;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.geoaiti.database.util.NameDescriptionTranslator;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "users")
public class PermissionTranslator extends NameDescriptionTranslator {

    @NotNull
    @Column(length = 100)
    private String app;

    @NotNull
    @Column(length = 100)
    private String feature;

    @NotNull
    @Column(length = 100)
    private String display;

    @ManyToOne
    @JoinColumn
    private Permission permission;

}
