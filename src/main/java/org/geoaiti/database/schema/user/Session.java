package org.geoaiti.database.schema.user;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.geoaiti.database.util.Auditable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "users")
public class Session extends Auditable {

    @Column(columnDefinition = "text")
    private String jwt;

    @NotNull
    private Long date;

    @NotNull
    private Long expired;

    @Column(columnDefinition = "text")
    private String data;

    @ManyToOne
    @JoinColumn
    private User user;

}
