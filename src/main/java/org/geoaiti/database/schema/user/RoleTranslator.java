package org.geoaiti.database.schema.user;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.geoaiti.database.util.NameDescriptionTranslator;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "users")
public class RoleTranslator extends NameDescriptionTranslator {

    @NotNull
    @Column(length = 100)
    private String display;

    @ManyToOne
    @JoinColumn
    private Role role;

}
