package org.geoaiti.database.schema.user;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.geoaiti.database.schema.global.IdentityRoot;
import org.geoaiti.database.util.NameDescriptionAuditable;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "users")
public class User extends NameDescriptionAuditable {

    @NotNull
    @Column(length = 100, unique = true)
    private String username;

    @Column(unique = true)
    private String googleId;

    @NotNull
    @Email
    @Column(length = 100, unique = true)
    private String email;

    @NotNull
    @Column(length = 100, unique = true)
    private String phoneNumber;

    @NotNull
    @Column(columnDefinition = "text")
    private String password;

    private String profile;

    @OneToMany(mappedBy = "user")
    private List<UserRole> userRoles;

    @OneToMany(mappedBy = "user")
    private List<Session> sessions;

    @OneToMany(mappedBy = "user")
    private List<IdentityRoot> identities;

}
