package org.geoaiti.database.schema.pair;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.geoaiti.database.util.Auditable;

import java.util.List;
import java.util.UUID;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "pairs")
public class Pair extends Auditable {

    @NotNull
    @Column(length = 100)
    private String valSystem;

    private Integer num;

    private Boolean bool;

    @OneToMany(mappedBy = "pair")
    private List<PairTranslator> translators;

    @OneToMany(mappedBy = "pair", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Pair> pairs;

    @ManyToOne
    @JoinColumn
    private Pair pair;

    @NotNull
    private UUID identityRoot;
}
