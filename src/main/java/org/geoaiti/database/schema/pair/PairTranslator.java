package org.geoaiti.database.schema.pair;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.geoaiti.database.util.NameDescriptionTranslator;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(schema = "pairs")
public class PairTranslator extends NameDescriptionTranslator {

    @NotNull
    @Column(length = 100)
    private String groups;

    @Column(columnDefinition = "text")
    private String valText;

    @ManyToOne
    @JoinColumn
    private Pair pair;

}
