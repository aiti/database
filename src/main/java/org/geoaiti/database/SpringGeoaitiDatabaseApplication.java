package org.geoaiti.database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGeoaitiDatabaseApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringGeoaitiDatabaseApplication.class, args);
	}
}
