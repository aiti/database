package org.geoaiti.database.util;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CompanyAddress {
    private String street;
    private String city;
    private String state;
    private String country;
    private String zipCode;
}
