package org.geoaiti.database.util;

public enum CompanyType {
    PUBLIC,
    PRIVATE,
    NONPROFIT,
    GOVERNMENT
}
