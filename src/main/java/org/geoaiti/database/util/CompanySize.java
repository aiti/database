package org.geoaiti.database.util;

public enum CompanySize {
    SELF_EMPLOYED,
    SMALL,
    MEDIUM,
    LARGE
}
