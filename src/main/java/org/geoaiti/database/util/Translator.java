package org.geoaiti.database.util;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Setter
@Getter
@MappedSuperclass
public class Translator extends Id {

    @NotNull
    @Column(length = 100)
    private String country;

    @NotNull
    @Column(length = 100)
    private String language;

}
