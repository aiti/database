package org.geoaiti.database.util;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@MappedSuperclass
public class NameDescriptionTranslator extends Translator {

    @NotNull
    @Column(length = 100)
    private String name;

    @Column(columnDefinition = "text")
    private String description;

}
